import View, { ViewClass, ViewOptions } from './View';

export type LazyViewPromise = Promise<{ default: ViewClass }>;

export interface LazyViewOptions extends ViewOptions {}

export default abstract class LazyView extends View {
  constructor(options: LazyViewOptions) {
    super(options);

    this.load().then(({ default: viewClass }) => {
      this.getComponentsNode().setViewClass(viewClass, true);
      new viewClass(options);
    });
  }

  abstract load(): LazyViewPromise;
}
