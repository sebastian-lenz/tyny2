import createElement, { CreateElementOptions } from './utils/createElement';
import dasherize from './utils/dasherize';
import isHtmlElement from './utils/isHtmlElement';
import View from './View';

export type Source = Object | Element;

export interface Reader {
  getValue(name: string, options: Param): any;
  hasValue(name: string, options: Param): boolean;
}

export class AttributeReader implements Reader {
  element: HTMLElement;

  constructor(element: HTMLElement) {
    this.element = element;
  }

  getValue(name: string, options: Param): any {
    const { attribute = `data-${dasherize(name)}` } = options;
    return this.element.getAttribute(attribute);
  }

  hasValue(name: string, options: Param): boolean {
    const { attribute = `data-${dasherize(name)}` } = options;
    return this.element.hasAttribute(attribute);
  }
}

export class ObjectReader implements Reader {
  object: any;

  constructor(object: any) {
    this.object = object;
  }

  getValue(name: string, options: Param): any {
    const { property = name } = options;
    return this.object[property];
  }

  hasValue(name: string, options: Param): boolean {
    const { property = name } = options;
    return property in this.object;
  }
}

export interface Param<T = any> {
  attribute?: string;
  defaultValue?: T | { (): T };
  property?: string;
  name: string;
}

export type Safe<T extends Param<U> = Param<any>, U = any> = T & {
  defaultValue: U | { (): U };
};

export interface EnumArgOptions<T> extends Param<T[keyof T]> {
  enum: T;
}

export interface InstanceArgOptions<T> extends Param<T> {
  ctor: { new (data: any): T };
}

export function toBoolean(value: any): boolean {
  if (typeof value === 'string') {
    value = value.toLowerCase();
    return value == 'true' || value == 'yes' || value == '1';
  }

  return !!value;
}

export function toInteger(value: any): number | undefined {
  if (typeof value === 'string') {
    return parseInt(value);
  } else if (typeof value === 'number') {
    return Math.round(value);
  }

  return undefined;
}

export function toNumber(value: any): number | undefined {
  if (typeof value === 'string') {
    return parseFloat(value);
  } else if (typeof value === 'number') {
    return value;
  }

  return undefined;
}

export function toString(value: any): string {
  return `${value}`;
}

export function toEnum<T extends Object>(
  enumType: T,
  value: any
): T[keyof T] | undefined {
  if (enumType.hasOwnProperty(value)) {
    return value;
  }

  if (typeof value === 'string') {
    value = value.toLowerCase();
    for (let key in enumType) {
      const enumValue = enumType[key];
      if (typeof enumValue === 'string') {
        if (enumValue.toLowerCase() === value.toLowerCase()) {
          return parseInt(key) as any;
        }
      } else if (value === enumValue) {
        return enumValue;
      }
    }
  }

  return undefined;
}

export default class Params {
  readonly readers: Array<Reader>;
  readonly view: View;

  constructor(view: View, ...sources: Source[]) {
    this.view = view;
    this.readers = sources.map(source => {
      return isHtmlElement(source)
        ? new AttributeReader(source)
        : new ObjectReader(source);
    });
  }

  read<T>(options: Param<T>): T | undefined {
    const { defaultValue, name } = options;
    const { readers } = this;

    for (let index = 0; index < readers.length; index++) {
      const reader = readers[index];
      if (reader.hasValue(name, options)) {
        return reader.getValue(name, options);
      }
    }

    return typeof defaultValue === 'function'
      ? (defaultValue as any)()
      : defaultValue;
  }

  bool(options: Param<boolean>): boolean {
    return toBoolean(this.read(options));
  }

  element<T extends HTMLElement>(
    options: Param<string | T> & CreateElementOptions & { tagName: string }
  ): T;
  element<T extends HTMLElement>(
    options: Param<string | T> & CreateElementOptions
  ): T | undefined;
  element<T extends HTMLElement>(
    options: Param<string | T> & CreateElementOptions
  ): T | undefined {
    const value = this.read(options);
    if (isHtmlElement(value)) {
      return value;
    }

    const { view } = this;
    if (view) {
      if (typeof value === 'string') {
        const element = view.query(value);
        if (element) {
          return <T>element;
        }
      }

      if (options.tagName) {
        return <T>createElement({
          attributes: options.attributes,
          appendTo: view.element,
          className: options.className,
          tagName: options.tagName,
        });
      }
    }

    return undefined;
  }

  enum<T>(options: Safe<EnumArgOptions<T>>): T[keyof T];
  enum<T>(options: EnumArgOptions<T>): T[keyof T] | undefined;
  enum<T>(options: EnumArgOptions<T>): T[keyof T] | undefined {
    const result = this.read(options);
    return result === undefined ? result : toEnum(options.enum, result);
  }

  instance<T>(options: InstanceArgOptions<T>): T {
    const { ctor } = options;
    const value = this.read(options);
    return value instanceof ctor ? value : new ctor(value);
  }

  int(options: Safe<Param<number>>): number;
  int(options: Param<number>): number | undefined;
  int(options: Param<number>): number | undefined {
    const result = this.read(options);
    return result === undefined ? result : toInteger(result);
  }

  number(options: Safe<Param<number>>): number;
  number(options: Param<number>): number | undefined;
  number(options: Param<number>): number | undefined {
    const result = this.read(options);
    return result === undefined ? undefined : toNumber(result);
  }

  string(options: Safe<Param<string>>): string;
  string(options: Param<string>): string | undefined;
  string(options: Param<string>): string | undefined {
    const result = this.read(options);
    return result === undefined ? result : toString(result);
  }
}
