export interface BoundingBoxType {
  xMin: number;
  xMax: number;
  yMin: number;
  yMax: number;
}

export interface DimensionsType {
  width: number;
  height: number;
}

export interface IntervalType {
  max: number;
  min: number;
}

export interface PointType {
  x: number;
  y: number;
}

export interface SimpleTransformType {
  x: number;
  y: number;
  rotation: number;
  scale: number;
}
