export interface CreateElementOptions {
  appendTo?: HTMLElement;
  attributes?: { [name: string]: string };
  className?: string;
  innerHTML?: string;
  tagName?: string;
}

export default function createElement(
  options: CreateElementOptions
): HTMLElement {
  const {
    appendTo,
    attributes,
    className,
    innerHTML,
    tagName = 'div',
  } = options;

  const element = document.createElement(tagName);

  if (appendTo) appendTo.appendChild(element);
  if (className) element.className = className;
  if (innerHTML) element.innerHTML = innerHTML;
  if (attributes) {
    Object.keys(attributes).forEach((key) => {
      const value = attributes[key];
      if (value !== null && typeof value !== 'undefined') {
        element.setAttribute(key, attributes[key]);
      }
    });
  }

  return element;
}
