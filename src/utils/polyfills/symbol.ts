let NativeSymbol: typeof Symbol;
let isNativeSafe: boolean;

if (typeof Symbol === 'function') {
  NativeSymbol = Symbol;
  try {
    String(NativeSymbol());
    isNativeSafe = true;
  } catch (ignore) {}
} else {
  class Symbol {
    private name: string;

    constructor(name: string) {
      this.name = name;
    }

    toString(): string {
      return this.name;
    }

    static iterator = new Symbol('iterator');
  }

  (<any>window)['Symbol'] = Symbol;
}
