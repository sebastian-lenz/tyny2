import before from './before';

/**
 * Creates a function that is restricted to invoking `func` once. Repeat calls
 * to the function return the value of the first invocation. The `func` is
 * invoked with the `this` binding and arguments of the created function.
 */
export default function once<T extends (...args: any[]) => any>(func: T): T {
  return before(2, func);
}
