import isEventSupported from '../isEventSupported';
import memoize from '../memoize';

export interface WheelProps {
  eventName: string;
}

export default memoize(function wheelProps(): WheelProps {
  return {
    eventName: isEventSupported('wheel') ? 'wheel' : 'mousewheel',
  };
});
