/**
 * Creates a function that invokes `func`, with the `this` binding and arguments
 * of the created function, while it's called less than `n` times. Subsequent
 * calls to the created function return the result of the last `func` invocation.
 */
export default function<TFunc extends (...args: any[]) => any>(
  n: number,
  callback: TFunc
): TFunc {
  let result: any;
  let func: TFunc | undefined = callback;
  if (typeof func != 'function') {
    throw new TypeError('Expected a function');
  }

  return <TFunc>function(this: any, ...args) {
    if (func) {
      result = func.apply(this, args);
    }

    if (--n <= 1) {
      func = undefined;
    }

    return result;
  };
}
