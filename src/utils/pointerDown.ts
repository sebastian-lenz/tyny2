export default function pointerDown() {
  if ('PointerEvent' in window) {
    return 'pointerdown';
  } else if ('ontouchstart' in window) {
    return 'touchstart';
  }

  return 'mousedown';
}
