export interface RequestOptions {
  method?: 'GET';
  headers?: any;
  params?: any;
  url: string;
}

export default function request({
  method = 'GET',
  headers,
  params,
  url,
}: RequestOptions): Promise<string> {
  return new Promise(function(resolve, reject) {
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);

    xhr.onload = function() {
      if (this.status >= 200 && this.status < 300) {
        resolve(`${xhr.response}`);
      } else {
        reject({
          status: this.status,
          statusText: xhr.statusText,
        });
      }
    };

    xhr.onerror = function() {
      reject({
        status: this.status,
        statusText: xhr.statusText,
      });
    };

    if (params && typeof params === 'object') {
      Object.keys(headers).forEach(function(key) {
        xhr.setRequestHeader(key, headers[key]);
      });
    }

    if (params && typeof params === 'object') {
      params = Object.keys(params)
        .map(function(key) {
          return (
            encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
          );
        })
        .join('&');
    }

    xhr.send(params);
  });
}
