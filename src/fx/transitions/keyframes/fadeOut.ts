import keyframes from '../../keyframes';
import memoize from '../../../utils/memoize';

export default memoize(function fadeOut(): string {
  return keyframes('tynyFadeOutKeyframes', {
    from: {
      opacity: '1',
      visibility: 'inherit',
    },
    to: {
      opacity: '0',
    },
  });
});
