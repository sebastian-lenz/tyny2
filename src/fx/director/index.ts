import Director from './Director';

let instance: Director;

export default function director(): Director {
  if (!instance) instance = new Director();
  return instance;
}
