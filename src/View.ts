import components, { ComponentsNode } from './services/components';
import createElement, { CreateElementOptions } from './utils/createElement';
import Delegate, { DelegatedEvent } from './Delegate';
import Params from './Params';
import PointerList from './pointers/PointerList';
import viewport from './services/viewport';
import { IntervalType } from './types';

function ensureElement(options: ViewOptions): HTMLElement {
  return !options.element ? createElement(options) : options.element;
}

export interface ViewClass<TView extends View = View> {
  new (options: any): TView;
}

export { DelegatedEvent };

export type MaybeView = View | undefined;

export interface ViewOptions extends CreateElementOptions {
  [name: string]: any;
  componentsNode?: ComponentsNode;
  element?: HTMLElement;
  owner?: View;
  template?: string | Function;
}

export interface CreateChildOptions<T extends View> {
  options?: any;
  selector: string;
  viewClass: ViewClass<T>;
}

/**
 * Base class of all views.
 */
export default class View extends Delegate {
  /**
   * The underlying DOM element of this view.
   */
  readonly element!: HTMLElement;

  readonly options: ViewOptions;

  /**
   * The component registry node linked to this view.
   * @private
   */
  private _componentsNode: ComponentsNode | undefined;

  /**
   * The pointer list attached to this view.
   * @private
   */
  private _pointerList: PointerList | undefined;

  /**
   * The default class name prefix used by predefined components.
   */
  static classNamePrefix: string = 'tyny';

  /**
   * View constructor.
   */
  constructor(options: ViewOptions = {}) {
    super(ensureElement(options));
    this.options = options;

    const { element } = this;
    const {
      appendTo,
      componentsNode = components().createNode(element),
      template,
    } = options;

    if (template) {
      if (typeof template === 'function') {
        element.innerHTML = template(this, options);
      } else {
        element.innerHTML = template;
      }
    }

    if (appendTo && element.parentElement !== appendTo) {
      appendTo.appendChild(element);
    }

    this._componentsNode = componentsNode;
    componentsNode.setView(this);
  }

  /**
   * Add a css class to the underlying dom element.
   */
  addClass(...tokens: string[]): this {
    this.element.classList.add(...tokens);
    return this;
  }

  /**
   * Dispose this view.
   *
   * Do not overwrite this method, use the `View.handleDispose` callback instead.
   */
  dispose() {
    const { element, _componentsNode, _pointerList } = this;

    if (_componentsNode) {
      this._componentsNode = undefined;
      _componentsNode.dispose();
    } else {
      this.handleDispose();
      super.dispose();

      if (_pointerList) {
        _pointerList.dispose();
        this._pointerList = undefined;
      }

      if (element && element.parentNode) {
        element.parentNode.removeChild(element);
      }
    }
  }

  params(): Params {
    return new Params(this, this.options, this.element);
  }

  query<T extends HTMLElement = HTMLElement>(selectors: string): T {
    return <T>this.element.querySelector(selectors);
  }

  queryAll<T extends HTMLElement = HTMLElement>(selectors: string): T[] {
    return Array.prototype.slice.call(this.element.querySelectorAll(selectors));
  }

  createChild<T extends View>({
    options = {},
    selector,
    viewClass,
  }: CreateChildOptions<T>): T {
    return new viewClass({
      element: this.query(selector),
      owner: this,
      ...options,
    });
  }

  createOptionalChild<T extends View>({
    options = {},
    selector,
    viewClass,
  }: CreateChildOptions<T>): T | null {
    const element = this.query(selector);
    if (!element) return null;

    return new viewClass({
      element,
      owner: this,
      ...options,
    });
  }

  createChildren<T extends View>({
    options = {},
    selector,
    viewClass,
  }: CreateChildOptions<T>): T[] {
    return this.queryAll(selector).map(
      element => new viewClass({ element, owner: this, ...options })
    );
  }

  /**
   * Return the component node of this View.
   */
  getComponentsNode(): ComponentsNode {
    let { _componentsNode } = this;
    if (!_componentsNode) {
      _componentsNode = components().createNode(this.element);
      _componentsNode.setView(this);
      this._componentsNode = _componentsNode;
    }

    return _componentsNode;
  }

  getVisibilityBounds(): IntervalType {
    const { element } = this;
    const bounds = element.getBoundingClientRect();
    const min = bounds.top + viewport().scrollTop;
    const max = min + (bounds.height || element.offsetHeight);
    return { min, max };
  }

  /**
   * Test whether the dom element has the given css class.
   */
  hasClass(token: string): boolean {
    return this.element.classList.contains(token);
  }

  /**
   * Remove a css class from the underlying dom element.
   */
  removeClass(...tokens: string[]): this {
    this.element.classList.remove(...tokens);
    return this;
  }

  /**
   * Toggle a css class on the underlying dom element.
   */
  toggleClass(token: string, force?: boolean): boolean {
    const { classList } = this.element;

    return typeof force === 'boolean'
      ? classList.toggle(token, force)
      : classList.toggle(token);
  }

  protected handleDispose() {}
}
