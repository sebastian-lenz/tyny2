import Breakpoints from './Breakpoints';
import BreakpointsEvent from './BreakpointsEvent';

let instance: Breakpoints;

export { BreakpointsEvent };

export interface Breakpoint {
  containerWidth: number;
  minWidth: number;
  name: string;
  update?: { (breakpoint: Breakpoint, width: number): void };
}

export default function breakpoints(): Breakpoints {
  if (!instance) instance = new Breakpoints();
  return instance;
}
