import EventBase, { EventOptions } from '../../Event';
import Viewport from './Viewport';

export interface ViewportEventOptions extends EventOptions<Viewport> {
  nativeEvent?: Event;
}

export default class ViewportEvent extends EventBase<Viewport> {
  readonly hasScrollbars: boolean;
  readonly height: number;
  readonly nativeEvent: Event | null;
  readonly scrollLeft: number;
  readonly scrollTop: number;
  readonly width: number;

  static readonly measureEvent: string = 'measure';
  static readonly pointerDownEvent: string = 'pointerDown';
  static readonly resizeEvent: string = 'resize';
  static readonly scrollbarsEvent: string = 'scrollbars';
  static readonly scrollEvent: string = 'scroll';

  constructor(options: ViewportEventOptions) {
    super(options);

    const { nativeEvent = null, target } = options;
    this.hasScrollbars = target.hasScrollbars();
    this.height = target.height;
    this.nativeEvent = nativeEvent;
    this.scrollLeft = target.scrollLeft;
    this.scrollTop = target.scrollTop;
    this.width = target.width;
  }
}
