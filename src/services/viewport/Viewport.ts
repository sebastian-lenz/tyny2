import Delegate from '../../Delegate';
import dispatcher, { DispatcherEvent } from '../dispatcher';
import ViewportEvent from './ViewportEvent';

/**
 * Test whether pageXOffset and pageYOffset are available on the window object.
 */
const supportsPageOffset = 'pageXOffset' in window;

/**
 * Test whether stupid IE is in css compat mode.
 */
const isCSS1Compat = (document.compatMode || '') === 'CSS1Compat';

function pointerDown() {
  if ('PointerEvent' in window) {
    return 'pointerdown';
  } else if ('ontouchstart' in window) {
    return 'touchstart';
  }

  return 'mousedown';
}

/**
 * A service that triggers events on browser resize and document scroll actions.
 *
 * Both the resize and scrolling events will be synchronized with an animation frame
 * event to speed up page rendering.
 */
export default class Viewport extends Delegate {
  // The height of the viewport.
  height: number = 0;

  // The size of the scrollbar. Only available after disabling the scrollbars once.
  scrollBarSize: number = Number.NaN;

  // The horizontal scroll position of the document.
  scrollLeft: number = 0;

  // The vertical scroll position of the document.
  scrollTop: number = 0;

  // The width of the viewport.
  width: number = 0;

  // Whether the scroll position of the document has changed or not.
  protected hasScrollChanged: boolean = false;

  // Whether the size of the viewport has changed or not.
  protected hasSizeChanged: boolean = false;

  protected isInitialized: boolean = false;

  // A list of initiators that have disabled the scrollbars of the document.
  protected scrollInitiators: any[] = [];

  protected scrollRestorePosition: number = 0;

  /**
   * Viewport constructor.
   */
  constructor() {
    super(window);

    this.delegate('resize', this.handleResize);
    this.delegate('scroll', this.handleScroll);
    this.delegate(pointerDown(), this.handlePointerDown);
    this.listenTo(dispatcher(), DispatcherEvent.frameEvent, this.handleFrame);
  }

  /**
   * Disable the document scrollbars.
   *
   * @param initiator
   *   An identifier of the service or class that disables the scrollbars.
   *   Same value must be passed to enableScrolling, used to manage multiple
   *   scripts that turn scrolling on or off.
   */
  disableScrollbars(initiator: any) {
    const { scrollInitiators } = this;
    let { scrollBarSize } = this;

    const index = scrollInitiators.indexOf(initiator);
    if (index === -1) {
      scrollInitiators.push(initiator);
    }

    if (scrollInitiators.length === 1) {
      const { body } = document;
      const { style } = body;
      const { scrollTop } = this;

      if (isNaN(scrollBarSize)) {
        const { offsetWidth } = body;
        style.width = '100%';
        style.position = 'fixed';
        scrollBarSize = body.offsetWidth - offsetWidth;
        this.scrollBarSize = scrollBarSize;
      } else {
        style.width = '100%';
        style.position = 'fixed';
      }

      style.paddingRight = `${scrollBarSize}px`;
      style.top = `-${scrollTop}px`;

      this.scrollRestorePosition = scrollTop;
      this.handleResize();
      this.emit(
        new ViewportEvent({
          target: this,
          type: ViewportEvent.scrollbarsEvent,
        })
      );
    }
  }

  /**
   * Enable the document scrollbars.
   *
   * @param initiator
   *   An identifier of the service or class that enabled the scrollbars.
   */
  enableScrollbars(initiator: any) {
    const { scrollInitiators } = this;
    const index = scrollInitiators.indexOf(initiator);
    if (index !== -1) {
      scrollInitiators.splice(index, 1);
    }

    if (scrollInitiators.length === 0) {
      const { style } = document.body;
      style.position = '';
      style.paddingRight = '';
      style.top = '';
      style.width = '';
      window.scrollTo(this.scrollLeft, this.scrollRestorePosition);

      this.handleResize();
      this.emit(
        new ViewportEvent({
          target: this,
          type: ViewportEvent.scrollbarsEvent,
        })
      );
    }
  }

  hasScrollbars(): boolean {
    return this.scrollInitiators.length === 0;
  }

  setScrollLeft(value: number) {
    if (this.scrollLeft === value) return;
    window.scrollTo(value, this.scrollTop);
  }

  setScrollTop(value: number) {
    if (this.scrollTop === value) return;
    window.scrollTo(this.scrollLeft, value);
  }

  /**
   * Trigger a resize event.
   */
  triggerResize() {
    this.hasSizeChanged = true;
  }

  /**
   * Triggered on every page repaint.
   */
  protected handleFrame() {
    if (!this.isInitialized) {
      this.handleResize();
      this.handleScroll();
      this.hasSizeChanged = true;
      this.hasScrollChanged = true;
      this.isInitialized = true;
      return;
    }

    if (this.hasSizeChanged) {
      this.hasSizeChanged = false;
      this.emit(
        new ViewportEvent({
          target: this,
          type: ViewportEvent.measureEvent,
        })
      );

      this.emit(
        new ViewportEvent({
          target: this,
          type: ViewportEvent.resizeEvent,
        })
      );
    }

    if (this.hasScrollChanged) {
      this.hasScrollChanged = false;
      this.emit(
        new ViewportEvent({
          target: this,
          type: ViewportEvent.scrollEvent,
        })
      );
    }
  }

  handlePointerDown(nativeEvent: Event) {
    this.emit(
      new ViewportEvent({
        nativeEvent,
        target: this,
        type: ViewportEvent.pointerDownEvent,
      })
    );
  }

  /**
   * Triggered after the size of the window has changed.
   */
  protected handleResize() {
    const doc = document.documentElement;
    const width = Math.max(doc ? doc.clientWidth : 0, 0);
    const height = Math.max(doc ? doc.clientHeight : 0, 0);

    if (this.width != width || this.height != height) {
      this.width = width;
      this.height = height;
      this.hasSizeChanged = true;
    }
  }

  /**
   * Triggered after the scroll position of the document has changed.
   */
  protected handleScroll() {
    const doc = document.documentElement || document.body;
    let scrollLeft = 0;
    let scrollTop = 0;

    if (!this.hasScrollbars()) {
      return;
    }

    if (supportsPageOffset) {
      scrollLeft = window.pageXOffset;
      scrollTop = window.pageYOffset;
    } else if (isCSS1Compat && doc) {
      scrollLeft = doc.scrollLeft;
      scrollTop = doc.scrollTop;
    } else {
      scrollLeft = document.body.scrollLeft;
      scrollTop = document.body.scrollTop;
    }

    if (this.scrollLeft != scrollLeft || this.scrollTop != scrollTop) {
      this.scrollLeft = scrollLeft;
      this.scrollTop = scrollTop;
      this.hasScrollChanged = true;
    }
  }
}
