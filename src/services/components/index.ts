import Components from './Components';
import ComponentsNode from './ComponentsNode';

let instance: Components;

export { ComponentsNode };

export default function components(): Components {
  if (!instance) instance = new Components();
  return instance;
}
