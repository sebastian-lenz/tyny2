import NativeHandler from './NativeHandler';
import View from '../../View';
import { Observer } from './Visibility';
import { VisibilityTarget } from '.';

export type NativeTarget = View & VisibilityTarget;

export interface NativeObserverOptions {
  handler: NativeHandler;
  target: NativeTarget;
}

export default class NativeObserver implements Observer {
  readonly handler: NativeHandler;
  readonly target: NativeTarget;

  constructor(options: NativeObserverOptions) {
    this.handler = options.handler;
    this.target = options.target;
  }

  update() {}
}
