import EventEmitter from '../../EventEmitter';
import { Observer } from './Visibility';
import { VisibilityTarget } from './index';

export default abstract class VisibilityHandler<
  T extends Observer = Observer
> extends EventEmitter {
  observers: Array<T> = [];

  abstract register(target: VisibilityTarget): T;

  getObserver(target: VisibilityTarget): T | undefined {
    return this.observers.find(observer => observer.target === target);
  }

  unregister(target: VisibilityTarget) {
    const { observers } = this;
    this.observers = observers.filter(observer => observer.target !== target);
  }

  update() {}
}
