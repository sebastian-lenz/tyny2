import ImageCrop from './index';
import Event, { EventOptions } from '../../Event';

export interface ImageCropEventOptions extends EventOptions<ImageCrop> {
  target: ImageCrop;
}

export default class ImageCropEvent extends Event<ImageCrop> {
  static loadEvent: string = 'load';

  constructor(options: ImageCropEventOptions) {
    super(options);
  }
}
