import Crop, { CropMode, CropOptions, CropResult } from './Crop';
import Image, { ImageOptions } from '../Image';
import ImageCropEvent from './ImageCropEvent';
import View, { ViewOptions } from '../../View';
import visibility, { VisibilityTarget } from '../../services/visibility';
import viewport, { ViewportEvent } from '../../services/viewport';

export { Crop, CropMode, CropOptions, ImageCrop, ImageCropEvent };

/**
 * Constructor options for the ImageWrap class.
 */
export interface ImageCropOptions extends ViewOptions {
  crop?: Crop | CropOptions;
  disableMaskResize?: boolean;
  disableResize?: boolean;
  disableVisibility?: boolean;
  focusX?: number;
  focusY?: number;
  imageOptions?: ImageOptions;
  maxScale?: number;
  maxWidth?: number;
  minScale?: number;
  minWidth?: number;
  mode?: CropMode;
}

/**
 * Wraps an Image view and allows to apply various scale modes to the image.
 */
export default class ImageCrop extends View implements VisibilityTarget {
  currentCrop: CropResult | null = null;
  crop: Crop;
  displayHeight: number = Number.NaN;
  displayWidth: number = Number.NaN;
  image: Image;
  inViewport: boolean = false;

  constructor(options: ImageCropOptions = {}) {
    super({
      className: `${View.classNamePrefix}ImageCrop`,
      ...options,
    });

    const params = this.params();
    let crop: Crop;
    if (options.crop) {
      crop =
        options.crop instanceof Crop ? options.crop : new Crop(options.crop);
    } else {
      crop = new Crop({
        disableMaskResize: params.bool({
          name: 'disableMaskResize',
          defaultValue: false,
        }),
        focusY: params.number({ name: 'focusY', defaultValue: 0.5 }),
        focusX: params.number({ name: 'focusX', defaultValue: 0.5 }),
        maxScale: params.number({
          name: 'maxScale',
          defaultValue: Number.MAX_VALUE,
        }),
        minScale: params.number({ name: 'minScale', defaultValue: 0 }),
        mode: params.enum({
          name: 'mode',
          enum: CropMode,
          defaultValue: CropMode.Cover,
        }),
      });
    }

    const { imageOptions = {} } = options;
    const image = new Image({
      ...imageOptions,
      appendTo: this.element,
      disableVisibility: true,
      element: this.query('img'),
      owner: this,
    });

    crop.width = image.naturalWidth;
    crop.height = image.naturalHeight;

    this.crop = crop;
    this.image = image;
    this.listenToOnce(image, 'load', this.handleImageLoad);

    if (!options.disableVisibility) {
      visibility().register(this);
    }

    if (!options.disableResize) {
      this.listenTo(viewport(), ViewportEvent.measureEvent, this.handleMeasure);
      this.getComponentsNode().setResizeHandler(this.handleResize);
    }
  }

  /**
   * Load the image.
   *
   * @returns
   *   A promise that resolves after the image has loaded.
   */
  load(): Promise<void> {
    this.inViewport = true;
    this.update();
    return this.image.load();
  }

  /**
   * Set whether the element of this view is currently within the visible bounds
   * of the viewport or not.
   *
   * @param inViewport
   *   TRUE if the element is visible, FALSE otherwise.
   */
  setInViewport(inViewport: boolean) {
    if (this.inViewport == inViewport) return;
    this.inViewport = inViewport;

    this.update();
    this.image.setInViewport(inViewport);
  }

  setDisplaySize(width: number, height: number) {
    this.displayHeight = height;
    this.displayWidth = width;
    this.image.setDisplaySize(width, height);
    this.update();
  }

  update() {
    if (isNaN(this.displayWidth) || isNaN(this.displayHeight)) {
      this.handleMeasure();
    }

    const { crop, displayHeight, displayWidth, element, image } = this;
    this.currentCrop = crop.apply(
      element,
      image.element,
      displayWidth,
      displayHeight
    );
  }

  /**
   * Triggered after the image has loaded.
   */
  handleImageLoad() {
    const { crop, element, image } = this;
    crop.width = image.naturalWidth;
    crop.height = image.naturalHeight;
    element.classList.add('loaded');

    this.update();
    this.emit(
      new ImageCropEvent({
        target: this,
        type: ImageCropEvent.loadEvent,
      })
    );
  }

  handleDispose() {
    visibility().unregister(this);
  }

  handleMeasure() {
    const { element } = this;
    this.displayHeight = element.offsetHeight;
    this.displayWidth = element.offsetWidth;
  }

  handleResize() {
    this.update();
  }
}
