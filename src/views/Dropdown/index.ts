import DropdownLabel, { DropdownLabelOptions } from './DropdownLabel';
import DropdownList, { Direction, DropdownListOptions } from './DropdownList';
import DropdownOption from './DropdownOption';
import DropdownEvent, { DropdownEventOptions } from './DropdownEvent';
import View, { ViewOptions, ViewClass } from '../../View';
import viewport, { ViewportEvent } from '../../services/viewport';

export { Dropdown, DropdownEvent, DropdownEventOptions };

export interface DropdownOptions extends ViewOptions {
  select?: string | HTMLSelectElement;
  labelClass?: ViewClass<DropdownLabel>;
  labelOptions?: Partial<DropdownLabelOptions>;
  listClass?: ViewClass<DropdownList>;
  listOptions?: Partial<DropdownListOptions>;
}

export default class Dropdown extends View {
  defaultDirection: Direction | undefined;
  isExpanded: boolean = false;
  label: DropdownLabel;
  list: DropdownList;
  navigate: boolean;
  select: HTMLSelectElement | undefined;
  selected: DropdownOption | null = null;

  constructor(options: DropdownOptions = {}) {
    super(options);

    const params = this.params();
    this.navigate = params.bool({ name: 'navigate', defaultValue: false });
    this.select = params.element<HTMLSelectElement>({
      name: 'select',
      tagName: 'select',
      defaultValue: 'select',
    });

    this.defaultDirection = params.enum({
      name: 'defaultDirection',
      enum: Direction,
    });

    const { element, select } = this;
    const { labelClass, labelOptions, listClass, listOptions } = {
      labelClass: DropdownLabel,
      labelOptions: {},
      listClass: DropdownList,
      listOptions: {},
      ...options,
    };

    const label = new labelClass({
      ...labelOptions,
      appendTo: element,
      owner: this,
    });

    const list = new listClass({
      ...listOptions,
      appendTo: element,
      owner: this,
    });

    element.tabIndex = -1;
    this.label = label;
    this.list = list;

    if (select) {
      list.sync(select);
      this.setSelected(list.options[select.selectedIndex] || null, true);
    } else {
      this.setSelected(null, true);
    }

    this.delegate('change', this.handleChange, { selector: 'select' });
    this.delegate('focusout', this.handleFocusOut);
    this.listenTo(
      viewport(),
      ViewportEvent.pointerDownEvent,
      this.handleViewportPointerDown
    );
  }

  setExpanded(value: boolean) {
    if (this.isExpanded === value) return;
    this.isExpanded = value;

    this.element.classList.toggle('expanded', value);
    this.list.setExpanded(value);
  }

  setSelected(option: DropdownOption | null, silent?: boolean) {
    const { label, list, select } = this;
    this.selected = option;

    if (select) {
      const index = list.options.findIndex((child) => child === option);
      select.selectedIndex = index;
    }

    label.setOption(option);
    list.options.forEach((child) => child.setSelected(child === option));
    this.setExpanded(false);

    if (!silent) {
      const event = new DropdownEvent({
        target: this,
        type: DropdownEvent.change,
        option: option,
      });

      this.emit(event);
      if (this.navigate && option && !event.isDefaultPrevented()) {
        window.location.href = option.value;
      }
    }
  }

  onLabelClick() {
    this.setExpanded(true);
  }

  onListClick(option: DropdownOption) {
    this.setSelected(option);
  }

  handleChange() {
    const { list, select } = this;
    if (select) {
      this.setSelected(list.options[select.selectedIndex] || null);
    }
  }

  handleViewportPointerDown(event: ViewportEvent) {
    const { element, isExpanded } = this;
    if (!isExpanded) return;

    let target = event.nativeEvent
      ? (event.nativeEvent.target as HTMLElement)
      : undefined;

    while (target) {
      if (target === element) return;
      target = <HTMLElement>target.parentNode;
    }

    this.setExpanded(false);
  }

  handleFocusOut(event: Event) {
    this.setExpanded(false);
  }
}
