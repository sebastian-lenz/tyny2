import Dropdown from './index';
import DropdownOption from './DropdownOption';
import View, { ViewOptions } from '../../View';

export interface DropdownLabelOptions extends ViewOptions {
  emptyLabel?: string;
  owner: Dropdown;
}

export default class DropdownLabel extends View {
  dropdown: Dropdown;
  emptyLabel: string;
  extraClass: string | null = null;
  option: DropdownOption | null = null;

  constructor(options: DropdownLabelOptions) {
    super({
      className: `${View.classNamePrefix}DropdownLabel`,
      ...options,
    });

    this.dropdown = options.owner;
    this.emptyLabel = this.params().string({
      name: 'emptyLabel',
      defaultValue: '-',
    });

    this.delegate('click', this.handleClick);
  }

  setExtraClass(extraClass: string) {
    if (this.extraClass === extraClass) return;
    if (this.extraClass) this.removeClass(this.extraClass);

    this.extraClass = extraClass;
    if (extraClass) this.addClass(extraClass);
  }

  setOption(option: DropdownOption | null) {
    if (this.option === option) return;

    const { element, emptyLabel } = this;
    element.innerHTML = option ? option.caption : emptyLabel;

    this.setExtraClass(option ? option.extraClass : 'empty');
    this.option = option;
  }

  handleClick(event: Event) {
    event.preventDefault();
    this.dropdown.onLabelClick();
  }
}
