import Image, { ImageOptions } from '../Image';
import Zoomer, { ZoomerOptions } from './index';

export interface ImageZommerOptions extends ZoomerOptions {
  imageOptions?: ImageOptions;
}

export default class ImageZommer extends Zoomer {
  border: number = 50;
  image: Image;

  constructor(options: ImageZommerOptions) {
    super(options);

    this.image = this.createImage(options.imageOptions);
  }

  createImage(options: ImageOptions = {}): Image {
    let image = this.createOptionalChild({
      selector: 'img',
      viewClass: Image,
      options: {
        ...options,
        disableVisibility: true,
        disableResize: true,
      },
    });

    if (!image) {
      image = new Image({
        ...options,
        appendTo: this.element,
        disableVisibility: true,
        disableResize: true,
      });
    }

    image.isInViewport = true;
    return image;
  }

  draw(): void {
    const { border: bounds, image, position, scale } = this;
    const { element, naturalHeight, naturalWidth } = image;
    const offset = bounds * scale;
    const height = naturalHeight * scale;
    const width = naturalWidth * scale;

    element.style.left = `${position.x + offset}px`;
    element.style.top = `${position.y + offset}px`;
    element.style.width = `${width}px`;
    element.style.height = `${height}px`;

    image.displayWidth = width;
    image.displayHeight = height;
    image.update();
  }

  getNativeHeight(): number {
    return this.image.naturalHeight + 2 * this.border;
  }

  getNativeWidth(): number {
    return this.image.naturalWidth + 2 * this.border;
  }
}
