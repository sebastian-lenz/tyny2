import Point from '../../types/Point';
import stop from '../../fx/stop';
import View, { ViewOptions } from '../../View';
import viewport, { ViewportEvent } from '../../services/viewport';
import WheelBehaviour from './WheelBehaviour';
import ZoomBehaviour from './ZoomBehaviour';
import { PointType, BoundingBoxType, IntervalType } from '../../types';

export interface ZoomerOptions extends ViewOptions {}

export default abstract class Zoomer extends View {
  fitPadding: number = 0;
  height: number = 0;
  position: Point = new Point();
  scale: number = 1;
  wheelBehaviour: WheelBehaviour;
  width: number = 0;
  zoomBehaviour: ZoomBehaviour;

  constructor(options: ZoomerOptions) {
    super(options);

    this.wheelBehaviour = new WheelBehaviour(this);
    this.zoomBehaviour = new ZoomBehaviour(this);

    setTimeout(() => {
      this.handleMeasure();
      this.fitToView();
    }, 0);

    this.getComponentsNode().setResizeHandler(this.handleResize);
    this.listenTo(viewport(), ViewportEvent.measureEvent, this.handleMeasure);
  }

  abstract draw(): void;

  fitToView() {
    const { height, fitPadding: padding, width } = this;
    const nativeHeight = this.getNativeHeight();
    const nativeWidth = this.getNativeWidth();
    const scale = Math.min(
      1,
      (height - padding * 2) / nativeHeight,
      (width - padding * 2) / nativeWidth
    );

    const displayWidth = nativeWidth * scale;
    const displayHeight = nativeHeight * scale;
    const x = (width - displayWidth) * 0.5;
    const y = (height - displayHeight) * 0.5;

    stop(this);
    this.setPosition({ x, y });
    this.setScale(scale);
  }

  abstract getNativeHeight(): number;

  abstract getNativeWidth(): number;

  getPositionBounds(scale: number = this.scale): BoundingBoxType {
    const { height, width } = this;
    const nativeHeight = this.getNativeHeight();
    const nativeWidth = this.getNativeWidth();
    const displayWidth = nativeWidth * scale;
    const displayHeight = nativeHeight * scale;
    const x = (width - displayWidth) * 0.5;
    const y = (height - displayHeight) * 0.5;

    return {
      xMax: Math.max(x, 0),
      xMin: Math.min(x, width - displayWidth),
      yMax: Math.max(y, 0),
      yMin: Math.min(y, height - displayHeight),
    };
  }

  getScaleBounds(): IntervalType {
    const { height, fitPadding: padding, width } = this;
    const nativeHeight = this.getNativeHeight();
    const nativeWidth = this.getNativeWidth();
    const scale = Math.min(
      1,
      (height - padding * 2) / nativeHeight,
      (width - padding * 2) / nativeWidth
    );

    return {
      min: scale,
      max: 1,
    };
  }

  handleMeasure() {
    const { element } = this;
    this.height = element.offsetHeight;
    this.width = element.offsetWidth;
  }

  handleResize() {
    this.fitToView();
  }

  limitPosition({ x, y }: PointType, scale: number = this.scale): Point {
    const { xMax, xMin, yMin, yMax } = this.getPositionBounds(scale);
    if (x < xMin) x = xMin;
    if (x > xMax) x = xMax;
    if (y < yMin) y = yMin;
    if (y > yMax) y = yMax;

    return new Point(x, y);
  }

  limitScale(scale: number): number {
    const { max, min } = this.getScaleBounds();
    if (scale < min) scale = min;
    if (scale > max) scale = max;

    return scale;
  }

  setPosition({ x, y }: PointType) {
    if (this.position.x === x && this.position.y === y) return;

    this.position.x = x;
    this.position.y = y;
    this.draw();
  }

  setScale(value: number) {
    if (this.scale === value) return;
    this.scale = value;
    this.draw();
  }
}
