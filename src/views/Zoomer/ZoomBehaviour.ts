import easeOutExpo from '../../fx/easings/easeOutExpo';
import Behaviour from '../../pointers/behaviours/Behaviour';
import momentum from '../../fx/momentum';
import PointerList from '../../pointers/PointerList';
import PointerListEvent from '../../pointers/PointerListEvent';
import stop from '../../fx/stop';
import tween from '../../fx/tween';
import Zoomer from '.';
import { PointType } from '../../types';

export interface ZoomBehaviourOptions {}

export default class ZoomBehaviour extends Behaviour {
  initialPosition: PointType = { x: 0, y: 0 };
  initialScale: number = 0;
  isActive: boolean = false;
  minPointers: number = 1;
  zoomer: Zoomer;

  constructor(zoomer: Zoomer, options: ZoomBehaviourOptions = {}) {
    super(PointerList.forView(zoomer));

    this.zoomer = zoomer;

    const { list } = this;
    this.listenTo(list, PointerListEvent.addEvent, this.handlePointerAdd);
    this.listenTo(list, PointerListEvent.removeEvent, this.handlePointerRemove);
    this.listenTo(list, PointerListEvent.updateEvent, this.handlePointerUpdate);
  }

  handlePointerAdd(event: PointerListEvent) {
    const { nativeEvent } = event;
    const { isActive, list, minPointers } = this;
    const numPointers = list.pointers.length + 1;
    if (numPointers < minPointers) {
      return;
    }

    if (nativeEvent) {
      nativeEvent.preventDefault();
    }

    if (!isActive) {
      this.handleTransformStart();
      this.isActive = true;
    }
  }

  handlePointerRemove() {
    if (this.isActive) {
      this.handleTransformEnd();
      this.isActive = false;
    }
  }

  handlePointerUpdate(event: PointerListEvent) {
    if (this.isActive) {
      const { nativeEvent } = event;
      if (nativeEvent) {
        nativeEvent.preventDefault();
      }

      this.handleTransform();
    }
  }

  handleTransformStart() {
    const {
      position: { x, y },
      scale,
    } = this.zoomer;

    this.initialPosition = { x, y };
    this.initialScale = scale;
    stop(this.zoomer);
  }

  handleTransform() {
    const { initialPosition, initialScale, list, zoomer } = this;
    const transform = list.getTransform();
    const center = list.getCenter();

    const { max, min } = zoomer.getScaleBounds();
    let scale = initialScale;
    scale *= transform.scale;

    if (!isFinite(scale)) scale = initialScale;
    if (scale < min) scale = min + (scale - min) * 0.25;
    if (scale > max) scale = max + (scale - max) * 0.25;

    const { xMax, xMin, yMax, yMin } = zoomer.getPositionBounds(scale);
    const { left, top } = zoomer.element.getBoundingClientRect();
    let x = center.x - left;
    let y = center.y - top;
    x += ((initialPosition.x - x) / initialScale) * scale + transform.x;
    y += ((initialPosition.y - y) / initialScale) * scale + transform.y;

    if (x < xMin) x = xMin + (x - xMin) * 0.5;
    if (x > xMax) x = xMax + (x - xMax) * 0.5;
    if (y < yMin) y = yMin + (y - yMin) * 0.5;
    if (y > yMax) y = yMax + (y - yMax) * 0.5;

    zoomer.setPosition({ x, y });
    zoomer.setScale(scale);
  }

  handleTransformEnd() {
    const { list, zoomer } = this;
    const { position, scale: initialScale } = zoomer;
    const scale = zoomer.limitScale(initialScale);

    if (list.pointers.length > 1) {
      const { left, top } = zoomer.element.getBoundingClientRect();
      const center = list.getCenter();
      let x = center.x - left;
      let y = center.y - top;
      x += ((position.x - x) / initialScale) * scale;
      y += ((position.y - y) / initialScale) * scale;

      tween(
        zoomer,
        {
          position: zoomer.limitPosition({ x, y }, scale),
          scale,
        },
        {
          easing: easeOutExpo,
        }
      );
    } else {
      const bounds = zoomer.getPositionBounds(scale);
      const velocity = list.velocity.get();

      momentum(zoomer, {
        position: {
          velocity: { x: velocity.x, y: velocity.y },
          max: { x: bounds.xMax, y: bounds.yMax },
          min: { x: bounds.xMin, y: bounds.yMin },
        },
      });
    }
  }
}
