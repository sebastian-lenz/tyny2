import CycleableView from './index';
import Event, { EventOptions } from '../../Event';
import View from '../../View';

export interface CycleableViewEventOptions extends EventOptions<CycleableView> {
  fromView?: View;
  options: any;
  toView?: View;
}

export default class CycleableViewEvent extends Event<CycleableView> {
  readonly fromView: View | undefined;
  readonly options: any;
  readonly toView: View | undefined;

  static changeEvent: string = 'change';

  constructor(options: CycleableViewEventOptions) {
    super(options);

    this.fromView = options.fromView;
    this.options = options.options;
    this.toView = options.toView;
  }
}
