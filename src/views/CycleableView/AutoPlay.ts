import CycleableView, { CycleableViewEvent } from './index';
import EventEmitter from '../../EventEmitter';

export interface AutoPlayOptions {
  autoStart?: boolean;
  interval?: number;
  target: CycleableView;
}

export default class AutoPlay extends EventEmitter {
  interval: number;

  readonly target: CycleableView;
  private timeout: number | undefined;

  constructor(options: AutoPlayOptions) {
    super();

    this.target = options.target;
    this.interval = options.interval || 5000;

    if (options.autoStart) {
      this.start();
    }

    this.listenTo(
      options.target,
      CycleableViewEvent.changeEvent,
      this.handleChange
    );
  }

  pause() {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = undefined;
    }
  }

  start() {
    const { handleTimeout, interval } = this;
    if (this.timeout) {
      clearTimeout(this.timeout);
    }

    this.timeout = window.setTimeout(handleTimeout, interval);
  }

  private handleChange() {
    if (this.timeout) {
      this.start();
    }
  }

  private handleTimeout = () => {
    this.timeout = undefined;

    const { target } = this;
    target.setCurrentIndex(target.getCurrentIndex() + 1);

    this.start();
  };
}
