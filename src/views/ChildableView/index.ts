import ChildableViewEvent from './ChildableViewEvent';
import View, { ViewOptions, ViewClass } from '../../View';

export { ChildableViewEvent };

/**
 * Constructor options for the ChildableView class.
 */
export interface ChildableViewOptions extends ViewOptions {
  /**
   * A css selector or an element used as the container for all children.
   */
  container?: string | HTMLElement;

  /**
   * A css selector used to discover existing children.
   */
  selector?: string;

  /**
   * Default view class used to construct children.
   */
  viewClass?: ViewClass;

  viewOptions?: ViewOptions;
}

/**
 * A view that contains a list of child views.
 *
 * @event "add" (child: ChildType, at: number): void
 *   Triggered after a child has been added to this view.
 * @event "remove" (child: ChildType, at: number): void
 *   Triggered after a child has been removed to this view.
 */
export default class ChildableView<TChild extends View = View> extends View {
  /**
   * The container children will be placed in.
   */
  container: HTMLElement;

  /**
   * Default view class used to construct children.
   */
  viewClass: ViewClass;

  viewOptions: ViewOptions;

  /**
   * The list of children attached to this view.
   */
  children: TChild[] = [];

  constructor(options: ChildableViewOptions = {}) {
    super(options);

    const params = this.params();
    const { selector, viewClass = View, viewOptions = {} } = options;

    this.viewClass = viewClass;
    this.viewOptions = viewOptions;
    this.container = params.element({ name: 'container' }) || this.element;

    if (selector) {
      const { children, container } = this;
      const elements = container.querySelectorAll(selector);
      for (let index = 0; index < elements.length; index++) {
        const element = <HTMLElement>elements[index];
        const child = this.createView(element);
        children.push(child);
      }
    }
  }

  /**
   * Add the given child view to this view.
   *
   * @param child
   *   The child view that should be added to this view.
   * @param at
   *   Optional index the child should be inserted at.
   */
  addChild(child: TChild, at?: number): this {
    const { children, container } = this;
    const count = children.length;

    if (at === void 0 || at >= count) {
      at = count;
      container.appendChild(child.element);
      children.push(child);
    } else if (at <= 0) {
      at = 0;
      container.insertBefore(child.element, container.firstChild);
      children.unshift(child);
    } else {
      container.insertBefore(child.element, children[at].element);
      children.splice(at, 0, child);
    }

    this.handleChildAdd(child, at);
    return this;
  }

  /**
   * Add the given list of child views to this view.
   *
   * @param children
   *   The list of child views that should be added to this view.
   * @param at
   *   Optional index the child should be inserted at.
   */
  addChildren(children: TChild[], at?: number): this {
    for (let index = 0, count = children.length; index < count; index++) {
      this.addChild(children[index], at ? at + index : at);
    }

    return this;
  }

  find(callback: {
    (child: TChild, index: number): boolean;
  }): TChild | undefined {
    return this.children.find(callback);
  }

  forEach(callback: { (child: TChild, index: number): void }) {
    this.children.forEach(callback);
  }

  getChild(index: number): TChild | undefined {
    return this.children[index];
  }

  getLength(): number {
    return this.children.length;
  }

  indexOf(child: TChild): number {
    return this.children.indexOf(child);
  }

  /**
   * Remove all children from this view.
   */
  removeAllChildren(): this {
    const { children } = this;
    let child: TChild | undefined;

    while ((child = children.pop())) {
      this.handleChildRemove(child, children.length);
      child.dispose();
    }

    children.length = 0;
    return this;
  }

  /**
   * Remove the given child view from this view.
   *
   * @param child
   *   The child view that should be removed.
   */
  removeChild(child: TChild): this {
    const { children } = this;
    const index = children.indexOf(child);
    if (index !== -1) {
      children.splice(index, 1);
      child.dispose();
      this.handleChildRemove(child, index);
    }

    return this;
  }

  /**
   * Remove the given list of child views from this view.
   *
   * @param children
   *   The list of child views that should be removed.
   */
  removeChildren(children: TChild[]): this {
    children.forEach(child => this.removeChild(child));
    return this;
  }

  /**
   * Create a new view instance for the given child element.
   *
   * @param element
   *   The dom element whose view instance should be created.
   * @returns
   *   A view instance for the given child element.
   */
  protected createView(element: HTMLElement): TChild {
    const { viewClass, viewOptions } = this;
    return new viewClass({
      ...viewOptions,
      appendTo: this.container,
      element,
      owner: this,
    }) as TChild;
  }

  protected handleChildAdd(view: TChild, index: number) {
    this.emit(
      new ChildableViewEvent({
        index,
        target: this,
        type: ChildableViewEvent.addEvent,
        view,
      })
    );
  }

  protected handleChildRemove(view: TChild, index: number) {
    this.emit(
      new ChildableViewEvent({
        index,
        target: this,
        type: ChildableViewEvent.removeEvent,
        view,
      })
    );
  }
}
