import createElement from '../../utils/createElement';
import CycleableView, { CycleableViewEvent } from '../CycleableView';
import NumerationEvent from './NumerationEvent';
import View, { ViewOptions } from '../../View';
import { ChildableViewEvent } from '../ChildableView';
import { DelegatedEvent } from '../../Delegate';
import { IntervalType } from '../../types';

const defaultItemTemplate: ItemTemplate = (index: number) => `${index}`;

export { NumerationEvent };

export type ItemTemplate = (index: number) => string;

export interface NumerationOptions extends ViewOptions {
  container?: HTMLElement | string;
  itemClass?: string;
  itemTagName?: string;
  itemTemplate?: ItemTemplate;
  selectedItemClass?: string;
  target?: CycleableView;
}

export default class Numeration extends View {
  container: HTMLElement;
  itemClass: string;
  items: HTMLElement[] = [];
  itemTagName: string;
  itemTemplate: ItemTemplate;
  selectedItemClass: string;
  selectedRange: IntervalType = { min: -1, max: -1 };
  view: CycleableView | undefined;

  constructor(options: NumerationOptions = {}) {
    super({
      className: `${View.classNamePrefix}Numeration`,
      tagName: 'ul',
      ...options,
    });

    const {
      itemClass = `${View.classNamePrefix}Numeration--item`,
      itemTagName = 'li',
      itemTemplate = defaultItemTemplate,
      owner,
      selectedItemClass = 'selected',
      target,
    } = options;

    this.itemClass = itemClass;
    this.itemTagName = itemTagName;
    this.itemTemplate = itemTemplate;
    this.selectedItemClass = selectedItemClass;

    this.container =
      this.params().element({
        name: 'container',
      }) || this.element;

    if (target instanceof CycleableView) {
      this.setView(target);
    } else if (owner instanceof CycleableView) {
      this.setView(owner);
    }

    this.delegate('click', this.handleClick, { selector: `.${itemClass}` });
  }

  setView(view: CycleableView): this {
    if (this.view === view) return this;

    if (this.view) {
      this.stopListening(this.view);
    }

    if (view) {
      this.setLength(view.getLength())
        .setSelectedIndex(view.getCurrentIndex())
        .listenTo(
          view,
          CycleableViewEvent.changeEvent,
          this.handleCurrentChanged
        )
        .listenTo(view, ChildableViewEvent.addEvent, this.handleLengthChanged)
        .listenTo(
          view,
          ChildableViewEvent.removeEvent,
          this.handleLengthChanged
        );
    }

    this.view = view;
    return this;
  }

  setLength(length: number): this {
    const { container, itemClass, items, itemTagName, itemTemplate } = this;
    while (items.length < length) {
      items.push(
        createElement({
          appendTo: container,
          className: itemClass,
          tagName: itemTagName,
          innerHTML: itemTemplate(items.length + 1),
        })
      );
    }

    while (items.length > length) {
      const child = items.pop();
      if (child) {
        container.removeChild(child);
      }
    }

    this.setSelected(this.selectedRange);
    return this;
  }

  setSelectedIndex(index: number): this {
    return this.setSelected({ min: index, max: index });
  }

  setSelected(range: IntervalType): this {
    const { items, selectedRange, selectedItemClass } = this;
    const length = items.length - 1;
    let { min, max } = range;
    min = Math.max(-1, Math.min(length, Math.floor(min)));
    max = Math.max(-1, Math.min(length, Math.ceil(max)));

    if (max < min) {
      let tmp = min;
      min = max;
      max = tmp;
    }

    if (selectedRange.min == min && selectedRange.max == max) {
      return this;
    }

    for (let index = 0; index <= length; index++) {
      const value = index >= min && index <= max;
      items[index].setAttribute('aria-current', value ? 'true' : 'false');
      items[index].classList.toggle(selectedItemClass, value);
    }

    selectedRange.min = min;
    selectedRange.max = max;
    return this;
  }

  handleClick(event: DelegatedEvent) {
    const index = this.items.indexOf(<any>event.delegateTarget);
    if (index !== -1) {
      this.handleSelectIndex(index);
    }

    return true;
  }

  handleSelectIndex(index: number) {
    if (this.view) {
      this.view.setCurrentIndex(index);
    } else {
      this.emit(
        new NumerationEvent({
          index,
          target: this,
          type: NumerationEvent.changeEvent,
        })
      );
    }
  }

  handleCurrentChanged() {
    if (this.view) {
      this.setSelectedIndex(this.view.getCurrentIndex());
    }
  }

  handleLengthChanged() {
    if (this.view) {
      this.setLength(this.view.getLength());
    }
  }
}
