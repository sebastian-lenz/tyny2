import { SourceSetMode } from '../Image/SourceSet';
import Ratio, { RatioOptions } from './Ratio';

export type RatioSetSource = Array<Ratio | RatioOptions>;

export default class RatioSet {
  ratios: Ratio[];

  constructor(sources?: RatioSetSource) {
    if (sources) {
      this.ratios = sources.map((source) =>
        source instanceof Ratio ? source : new Ratio(source)
      );
    } else {
      this.ratios = [];
    }
  }

  get(width: number, height: number): Ratio | undefined {
    const { ratios } = this;
    const aspect = height / width;
    let bestRatio: Ratio | undefined;
    let bestScore: number = Number.MAX_VALUE;

    for (let index = 0; index < ratios.length; index++) {
      const ratio = ratios[index];
      const ratioAspect = ratio.height / ratio.width;
      const ratioDif = Math.abs(aspect - ratioAspect);

      const crop = ratio.getCrop(width, height);
      const source = ratio.sourceSet.getSource(crop.width);
      const scale =
        source && source.mode === SourceSetMode.Width
          ? crop.width / source.bias
          : 1;

      const score = ratioDif + Math.pow(1 - Math.max(1, scale), 2);

      if (score < bestScore) {
        bestRatio = ratio;
        bestScore = score;
      }
    }

    return bestRatio;
  }
}
