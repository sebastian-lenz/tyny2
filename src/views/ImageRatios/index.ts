import dissolve from '../../fx/transitions/dissolve';
import debounce from '../../utils/debounce';
import Ratio, { RatioOptions } from './Ratio';
import RatioSet, { RatioSetSource } from './RatioSet';
import Swap, { SwapOptions } from '../Swap';
import viewport, { ViewportEvent } from '../../services/viewport';
import View from '../../View';
import visibility, { VisibilityTarget } from '../../services/visibility';
import { ImageCrop, CropMode, ImageCropEvent } from '../ImageCrop';

export { Ratio, RatioOptions, RatioSet, RatioSetSource };

export interface ImageRatiosOptions extends SwapOptions {
  disableResize?: boolean;
  disableVisibility?: boolean;
  ratioSet?: RatioSet | RatioSetSource;
}

export default class ImageRatios
  extends Swap<ImageCrop>
  implements VisibilityTarget
{
  imageAttr: any;
  debouncedUpdate: Function;
  displayHeight: number = 0;
  displayWidth: number = 0;
  inViewport: boolean = false;
  isLoaded: boolean = false;
  ratioSet: RatioSet;

  constructor(options: ImageRatiosOptions) {
    super({
      className: `${View.classNamePrefix}ImageRatios`,
      transition: dissolve({ duration: 200, noPureFadeIn: true }),
      ...options,
      appendContent: true,
      disposeContent: true,
      transist: null,
    });

    this.ratioSet = this.getRatioSet();
    this.debouncedUpdate = debounce(this.update, 50);

    if (!options.disableVisibility) {
      visibility().register(this);
    }

    if (!options.disableResize) {
      this.listenTo(viewport(), ViewportEvent.measureEvent, this.handleMeasure);
      this.getComponentsNode().setResizeHandler(this.handleResize);
    }
  }

  getRatioSet(): RatioSet {
    const params = this.params();
    const cropOptions = {
      disableMaskResize: params.bool({ name: 'disableMaskResize' }),
      focusY: params.number({ name: 'focusY' }),
      focusX: params.number({ name: 'focusX' }),
      maxScale: params.number({ name: 'maxScale' }),
      minScale: params.number({ name: 'minScale' }),
      mode: params.enum({ name: 'mode', enum: CropMode }),
    };

    const ratios: Array<Ratio> = [];
    const sources = params
      .string({
        attribute: 'data-ratioset',
        defaultValue: '',
        name: 'ratioSet',
      })
      .split(';');

    sources.forEach((source) => {
      const match = /(\d+)x(\d+) (.*)/.exec(source);
      if (!match) {
        return;
      }

      ratios.push(
        new Ratio({
          ...cropOptions,
          height: parseInt(match[3]),
          sourceSet: match[1],
          width: parseInt(match[2]),
        })
      );
    });

    return new RatioSet(ratios);
  }

  load(): Promise<void> {
    this.inViewport = true;
    this.update();

    if (!this.content) {
      return Promise.resolve();
    }

    return this.content.load();
  }

  setDisplaySize(width: number, height: number) {
    this.displayHeight = height;
    this.displayWidth = width;

    const { content } = this;
    if (content) {
      content.setDisplaySize(width, height);
    }

    this.update();
  }

  setInViewport(inViewport: boolean) {
    if (this.inViewport === inViewport) return;
    this.inViewport = inViewport;
    this.update();

    const { content } = this;
    if (content) {
      content.setInViewport(inViewport);
    }
  }

  update() {
    const {
      content,
      displayHeight,
      displayWidth,
      element,
      imageAttr,
      inViewport,
      ratioSet,
    } = this;

    if (!inViewport) return;

    const crop = ratioSet.get(displayWidth, displayHeight);
    if (crop && (!content || content.crop !== crop)) {
      const image = new ImageCrop({
        appendTo: element,
        crop,
        disableResize: true,
        disableVisibility: true,
        imageOptions: {
          attributes: imageAttr,
          height: crop.height,
          sourceSet: crop.sourceSet,
          width: crop.width,
        },
      });

      image.setDisplaySize(displayWidth, displayHeight);

      if (content) this.stopListening(content);
      this.listenTo(image, ImageCropEvent.loadEvent, this.handleLoad);
      this.setContent(image);
    }
  }

  handleDispose() {
    visibility().unregister(this);
  }

  handleLoad() {
    if (this.isLoaded) return;
    this.isLoaded = true;
    this.addClass('loaded');
  }

  handleMeasure() {
    const { element } = this;
    this.displayHeight = element.offsetHeight;
    this.displayWidth = element.offsetWidth;
  }

  handleResize() {
    const { content, displayHeight, displayWidth } = this;
    if (content) {
      content.setDisplaySize(displayWidth, displayHeight);
    }

    this.debouncedUpdate();
  }
}
