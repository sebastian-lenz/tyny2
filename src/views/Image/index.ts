import ImageEvent from './ImageEvent';
import isImageLoaded from '../../utils/isImageLoaded';
import SourceSet, { SourceSetMode, SourceSetSource } from './SourceSet';
import View, { ViewOptions } from '../../View';
import visibility, { VisibilityTarget } from '../../services/visibility';
import viewport, { ViewportEvent } from '../../services/viewport';

export { SourceSet, SourceSetMode, SourceSetSource };

/**
 * Constructor options for the Image class.
 */
export interface ImageOptions extends ViewOptions {
  disableResize?: boolean;
  disableVisibility?: boolean;
  height?: number;
  sourceSet?: SourceSet | SourceSetSource;
  width?: number;
}

/**
 * Displays an image.
 */
export default class Image extends View implements VisibilityTarget {
  currentSource: string | null = null;
  displayHeight: number = Number.NaN;
  displayWidth: number = Number.NaN;
  isInViewport: boolean = false;
  loadPromise: Promise<void>;
  naturalHeight: number;
  naturalWidth: number;
  options!: ImageOptions;
  sourceSet: SourceSet;
  readonly element!: HTMLImageElement;

  /**
   * Image constructor.
   *
   * @param options
   *   The constructor options.
   */
  constructor(options: ImageOptions = {}) {
    super(
      (options = {
        className: `${View.classNamePrefix}Image`,
        ...options,
        tagName: 'img',
      })
    );

    const params = this.params();

    this.naturalHeight = params.int({
      attribute: 'height',
      defaultValue: Number.NaN,
      name: 'height',
    });

    this.naturalWidth = params.int({
      attribute: 'width',
      defaultValue: Number.NaN,
      name: 'width',
    });

    this.sourceSet = params.instance({
      attribute: 'data-srcset',
      ctor: SourceSet,
      name: 'sourceSet',
    });

    this.loadPromise = this.createPromise();

    if (!options.disableVisibility) {
      visibility().register(this);
    }

    if (!options.disableResize) {
      this.listenTo(viewport(), ViewportEvent.measureEvent, this.handleMeasure);
      this.getComponentsNode().setResizeHandler(this.handleResize);
    }
  }

  private createPromise() {
    const { element } = this;

    return new Promise<void>((resolve) => {
      const handleLoad = () => {
        this.undelegate('load', handleLoad);
        resolve();
      };

      if (isImageLoaded(element)) {
        resolve();
      } else {
        this.delegate('load', handleLoad);
      }
    }).then(() => {
      const { naturalHeight, naturalWidth } = element;

      if (isNaN(this.naturalHeight)) {
        this.naturalHeight = naturalHeight;
      }

      if (isNaN(this.naturalWidth)) {
        this.naturalWidth = naturalWidth;
      }

      element.classList.add('loaded');
      this.emit(
        new ImageEvent({
          target: this,
          type: ImageEvent.loadEvent,
        })
      );
    });
  }

  /**
   * Set whether the element of this view is currently within the visible bounds
   * of the viewport or not.
   *
   * @param inViewport
   *   TRUE if the element is visible, FALSE otherwise.
   */
  setInViewport(inViewport: boolean) {
    if (this.isInViewport === inViewport) return;
    this.isInViewport = inViewport;

    if (inViewport) {
      this.update();
    }
  }

  setDisplaySize(width: number, height: number) {
    this.displayHeight = height;
    this.displayWidth = width;
    this.update();
  }

  toOptions(): ImageOptions {
    return {
      disableResize: this.options.disableResize,
      disableVisibility: this.options.disableVisibility,
      height: this.naturalHeight,
      sourceSet: this.sourceSet,
      width: this.naturalWidth,
    };
  }

  /**
   * Update the source location of the image due to the current dimensions.
   */
  update() {
    if (isNaN(this.displayWidth)) {
      this.handleMeasure();
    }

    const { displayWidth, element, isInViewport, sourceSet } = this;
    if (isInViewport && sourceSet) {
      const source = sourceSet.get(displayWidth);
      if (this.currentSource !== source) {
        this.currentSource = source;
        element.src = source;
      }
    }
  }

  /**
   * Load the image.
   *
   * @returns
   *   A promise that resolves after the image has loaded.
   */
  load(): Promise<void> {
    this.isInViewport = true;
    this.update();
    return this.loadPromise;
  }

  protected handleDispose() {
    visibility().unregister(this);
  }

  protected handleMeasure() {
    const { element } = this;
    this.displayWidth = element.offsetWidth;
    this.displayHeight = element.offsetHeight;
  }

  protected handleResize() {
    this.update();
  }
}
