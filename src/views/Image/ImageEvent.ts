import Event, { EventOptions } from '../../Event';
import Image from './index';

export interface ImageEventOptions extends EventOptions<Image> {}

export default class ImageEvent extends Event<Image> {
  static loadEvent: string = 'load';

  constructor(options: ImageEventOptions) {
    super(options);
  }
}
