import View, { ViewOptions } from '../../View';
import { SelectableView } from '../../utils/isSelectableView';

export interface SwapContentOptions extends ViewOptions {}

export default class SwapContent extends View implements SelectableView {
  isSelected: boolean = false;

  setSelected(value: boolean) {
    this.isSelected = value;
    this.toggleClass('selected', value);
  }
}
