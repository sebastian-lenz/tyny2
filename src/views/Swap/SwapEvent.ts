import Event, { EventOptions } from '../../Event';
import Swap from './index';
import View from '../../View';

export interface SwapEventOptions extends EventOptions<Swap> {
  from?: View;
  to?: View;
}

export default class SwapEvent extends Event<Swap> {
  readonly from: View | undefined;
  readonly to: View | undefined;

  static transitionEndEvent: string = 'transitionEnd';
  static transitionStartEvent: string = 'transitionStart';

  constructor(options: SwapEventOptions) {
    super(options);
    this.from = options.from;
    this.to = options.to;
  }
}
