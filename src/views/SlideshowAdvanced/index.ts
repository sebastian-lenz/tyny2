import AutoPlay from '../CycleableView/AutoPlay';
import DragBehaviour, { DragBehaviourView } from './DragBehaviour';
import ImageCrop from '../ImageCrop';
import Picture from '../Picture';
import Slide from './Slide';
import SlideshowBase, { SlideshowOptions } from '../Slideshow';
import visibility, { VisibilityTarget } from '../../services/visibility';
import viewport, { ViewportEvent } from '../../services/viewport';

export type PeakMode = 'none' | 'previous' | 'next';

export interface SlideshowAdvancedOptions extends SlideshowOptions {
  autoPlayInterval?: number;
  disableDrag?: boolean;
  disableMouseDrag?: boolean;
  imageOptions?: any;
  imageSelector: string;
  imageViewClass: typeof ImageCrop | typeof Picture;
  viewport?: string | null;
}

export default class SlideshowAdvanced<TSlide extends Slide = Slide>
  extends SlideshowBase<TSlide>
  implements DragBehaviourView, VisibilityTarget {
  allowTransition: boolean = true;
  autoPlay: AutoPlay | null;
  disabled: boolean = false;
  dragBehaviour: DragBehaviour | null;
  isInViewport: boolean = false;
  offset: number = 0;
  peakMode: PeakMode = 'none';
  peakSlide: Slide | null = null;
  viewport: HTMLElement;
  viewportHeight: number = 0;
  viewportWidth: number = 0;

  constructor({
    autoPlayInterval = 6000,
    initialIndex = 0,
    imageOptions = {},
    imageSelector,
    imageViewClass,
    viewClass = Slide,
    viewOptions = {},
    ...options
  }: SlideshowAdvancedOptions) {
    super({
      viewClass,
      viewOptions: {
        imageOptions,
        imageSelector,
        imageViewClass,
        ...viewOptions,
      },
      ...options,
    });

    this.viewport = options.viewport
      ? this.query(options.viewport)
      : this.element;

    this.autoPlay = autoPlayInterval
      ? new AutoPlay({
          autoStart: true,
          interval: autoPlayInterval,
          target: this,
        })
      : null;

    const { children } = this;
    const preventDrag = options.disableDrag || children.length < 2;
    this.dragBehaviour = preventDrag
      ? null
      : new DragBehaviour({
          disableMouseDrag: options.disableMouseDrag,
          view: this as any,
        });

    if (children.length) {
      this.current = children[initialIndex];
      children[initialIndex].setSelected(true);
    }

    this.listenTo(viewport(), ViewportEvent.measureEvent, this.handleMeasure);
    visibility().register(this);
  }

  handleMeasure() {
    const { viewport } = this;
    this.viewportHeight = viewport.offsetHeight;
    this.viewportWidth = viewport.offsetWidth;
  }

  setAllowTransition(value: boolean) {
    const { allowTransition, autoPlay } = this;
    if (allowTransition === value) return;
    this.allowTransition = value;

    if (autoPlay) {
      if (value) {
        autoPlay.start();
      } else {
        autoPlay.pause();
      }
    }
  }

  setOffset(value: number) {
    if (value > 1) value = 1;
    if (value < -1) value = -1;
    if (this.offset === value) return;
    this.offset = value;

    if (value === 0) {
      this.setPeakMode('none');
    } else {
      this.setPeakMode(value < 0 ? 'next' : 'previous');
    }

    const { container } = this;
    container.style.transform = `translateX(${value * 100}%)`;
  }

  setPeakMode(value: PeakMode) {
    if (this.peakMode === value) return;
    this.peakMode = value;

    const { peakSlide } = this;
    if (peakSlide) peakSlide.setPeakMode('none');

    if (value === 'none') {
      this.peakSlide = null;
    } else {
      const slide =
        (value === 'next' ? this.getNext() : this.getPrevious()) || null;
      if (slide) slide.setPeakMode(value);
      this.peakSlide = slide;
    }
  }

  setInViewport(value: boolean): void {
    this.isInViewport = value;

    this.children.forEach((child) => child.updateVisibility());
  }
}
