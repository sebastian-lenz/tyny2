import ImageCrop from '../ImageCrop';
import Picture from '../Picture';
import SlideshowAdvanced, { PeakMode } from './index';
import ucfirst from '../../utils/ucfirst';
import View, { ViewOptions } from '../../View';
import { SelectableView } from '../../utils/isSelectableView';

export interface SlideOptions extends ViewOptions {
  imageOptions: any;
  imageSelector: string;
  imageViewClass: typeof ImageCrop | typeof Picture;
}

export default class Slide extends View implements SelectableView {
  isSelected: boolean = false;
  image: ImageCrop | Picture | null;
  peakMode: PeakMode = 'none';
  slideshow: SlideshowAdvanced<Slide>;

  constructor(options: SlideOptions) {
    super(options);

    this.slideshow = options.owner as SlideshowAdvanced<Slide>;
    this.image = this.createImage(options);
  }

  createImage(options: SlideOptions) {
    return this.createOptionalChild<ImageCrop | Picture>({
      selector: options.imageSelector,
      viewClass: options.imageViewClass,
      options: {
        ...options.imageOptions,
        disableVisibility: true,
      },
    });
  }

  load(): Promise<void> {
    const { image } = this;
    return image ? image.load() : Promise.resolve();
  }

  setPeakMode(value: PeakMode) {
    const { peakMode } = this;
    if (peakMode === value) return;
    this.peakMode = value;

    if (peakMode !== 'none') {
      this.removeClass(`peak${ucfirst(peakMode)}`);
    }

    if (value !== 'none') {
      this.addClass(`peak${ucfirst(value)}`);
      this.load();
    }
  }

  setSelected(value: boolean): void {
    this.isSelected = value;
    this.updateVisibility();
    this.toggleClass('isCurrent', value);
  }

  updateVisibility() {
    const { isSelected, image, slideshow } = this;
    if (image) {
      image.setInViewport(isSelected && slideshow.isInViewport);
    }
  }
}
