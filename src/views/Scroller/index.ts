import ScrollBehaviour, { ScrollableView } from './ScrollBehaviour';
import stop from '../../fx/stop';
import transformProps from '../../utils/vendors/transformProps';
import tween, { Tween, TweenOptions } from '../../fx/tween';
import View, { ViewOptions } from '../../View';
import viewport, { ViewportEvent } from '../../services/viewport';
import { PointType, BoundingBoxType } from '../../types';

export { ScrollableView, ScrollBehaviour };

export interface ScrollerOptions extends ViewOptions {
  content: HTMLElement | string;
  direction: 'horizontal' | 'vertical' | 'both';
  disableMeasure?: boolean;
  initialPosition?: PointType;
  useContentMargins?: boolean;
  viewport?: HTMLElement | string;
}

export default class Scroller extends View implements ScrollableView {
  behaviour: ScrollBehaviour;
  content: HTMLElement;
  currentTarget: PointType | null = null;
  currentTween: Tween | null = null;
  isMeasureDisabled: boolean;
  position: PointType;
  positionBounds: BoundingBoxType = { xMin: 0, xMax: 0, yMin: 0, yMax: 0 };
  useContentMargins: boolean = false;
  viewport: HTMLElement;

  constructor(options: ScrollerOptions) {
    super(options);

    const params = this.params();
    const { direction = 'both', initialPosition = { x: 0, y: 0 } } = options;

    this.content = params.element({ name: 'content' }) || this.element;
    this.isMeasureDisabled = !!options.disableMeasure;
    this.position = initialPosition;
    this.useContentMargins = !!options.useContentMargins;
    this.viewport = params.element({ name: 'viewport' }) || this.element;

    this.getComponentsNode().setResizeHandler(this.handleResize);

    if (!options.disableMeasure) {
      this.listenTo(viewport(), ViewportEvent.measureEvent, this.handleMeasure);
    }

    this.behaviour = new ScrollBehaviour({
      direction,
      view: this,
    });
  }

  clampPosition(value: PointType): PointType {
    const { xMin, xMax, yMin, yMax } = this.getPositionBounds();
    let { x, y } = value;

    if (x > xMax) x = xMax;
    if (x < xMin) x = xMin;
    if (y > yMax) y = yMax;
    if (y < yMin) y = yMin;

    return { x, y };
  }

  getPosition(): PointType {
    const { x, y } = this.position;
    return { x, y };
  }

  getPositionBounds(): BoundingBoxType {
    if (this.isMeasureDisabled) {
      this.handleMeasure();
    }

    return this.positionBounds;
  }

  gotoPosition(value: PointType) {
    stop(this);
    this.setPosition(this.clampPosition(value));
  }

  handleMeasure() {
    const {
      content,
      positionBounds: bounds,
      useContentMargins,
      viewport,
    } = this;

    bounds.xMin = 0;
    bounds.xMax = content.scrollWidth - viewport.offsetWidth;
    bounds.yMin = 0;
    bounds.yMax = content.scrollHeight - viewport.offsetHeight;

    if (useContentMargins) {
      const style = window.getComputedStyle(content);
      bounds.xMax +=
        parseFloat(style.marginLeft || '0') +
        parseFloat(style.marginRight || '0');
      bounds.yMax +=
        parseFloat(style.marginTop || '0') +
        parseFloat(style.marginBottom || '0');
    }
  }

  isPositionPaged(): boolean {
    return false;
  }

  setPosition(value: PointType): void {
    const { content } = this;
    const { x, y } = this.toDisplayOffset(value);
    const { transform } = transformProps();

    (<any>content.style)[transform] = `translate(${-x}px, ${-y}px)`;

    this.position.x = value.x;
    this.position.y = value.y;
  }

  toDisplayOffset(value: PointType): PointType {
    return value;
  }

  toLocalOffset(value: PointType): PointType {
    return value;
  }

  tweenTo(position: PointType, options: Partial<TweenOptions>): void {
    let { currentTween } = this;
    this.currentTarget = position;

    if (currentTween) {
      currentTween.advance({ position }, options);
    } else {
      currentTween = tween(
        this,
        { position },
        { ...options, rejectOnStop: true }
      );

      this.currentTween = currentTween;
      currentTween.then(
        () => (this.currentTween = this.currentTarget = null),
        () => (this.currentTween = this.currentTarget = null)
      );
    }
  }

  handleResize() {
    stop(this);
    this.setPosition(this.clampPosition(this.getPosition()));
  }
}
