import DragBehaviour from '../../pointers/behaviours/DragBehaviour';
import DragEvent from '../../pointers/behaviours/DragEvent';
import easeOutExpo from '../../fx/easings/easeOutExpo';
import momentum from '../../fx/momentum';
import Pointer from '../../pointers/Pointer';
import PointerList from '../../pointers/PointerList';
import stop from '../../fx/stop';
import View from '../../View';
import { PointType, BoundingBoxType } from '../../types';
import { TweenOptions } from '../../fx/tween';

export { DragEvent };

export interface ScrollableView extends View {
  clampPosition(value: PointType): PointType;
  getPosition(): PointType;
  getPositionBounds(): BoundingBoxType;
  isPositionPaged(): boolean;
  setPosition(value: PointType): void;
  toLocalOffset(value: PointType): PointType;
  tweenTo(value: PointType, options: Partial<TweenOptions>): void;
}

export interface ScrollBehaviourOptions {
  direction: 'horizontal' | 'vertical' | 'both';
  disableWheel?: boolean;
  view: ScrollableView;
}

export default class ScrollBehaviour extends DragBehaviour {
  initialPosition: PointType;
  isDraging: boolean = false;
  preventNextClick: boolean = false;
  view: ScrollableView;

  constructor(options: ScrollBehaviourOptions) {
    super(PointerList.forView(options.view));

    const { direction, view } = options;
    this.direction = direction;
    this.initialPosition = view.getPosition();
    this.view = view;

    view.delegate('click', this.handleClick, { capture: true, scope: this });

    if (!options.disableWheel) {
      view.delegate('wheel', this.handleWheel, { scope: this });
    }

    this.listenTo(this, DragEvent.dragStartEvent, this.handleDragStart)
      .listenTo(this, DragEvent.dragEvent, this.handleDrag)
      .listenTo(this, DragEvent.dragEndEvent, this.handleDragEnd);
  }

  private getVelocity(pointer: Pointer): PointType {
    const { direction, view } = this;
    const { clientX, clientY } = pointer.velocity.get();
    const velocity = { x: 0, y: 0 };
    if (direction !== 'vertical') {
      velocity.x = -clientX;
    }

    if (direction !== 'horizontal') {
      velocity.y = -clientY;
    }

    return view.toLocalOffset(velocity);
  }

  handleClick(event: Event) {
    if (this.preventNextClick) {
      event.preventDefault();
      event.stopPropagation();
      this.preventNextClick = false;
    }
  }

  private handleDragStart(event: DragEvent) {
    const { direction, view } = this;
    const { nativeEvent } = event.listEvent;
    stop(view);

    const { xMin, xMax, yMin, yMax } = view.getPositionBounds();
    const xDiff = Math.max(0, xMax - xMin);
    const yDiff = Math.max(0, yMax - yMin);
    const canScroll =
      (direction !== 'vertical' && xDiff > 0) ||
      (direction !== 'horizontal' && yDiff > 0);

    if (nativeEvent) {
      nativeEvent.preventDefault();
    }

    if (!canScroll) {
      event.preventDefault();
      return;
    }

    this.isDraging = true;
    this.initialPosition = view.getPosition();
  }

  private handleDrag(event: DragEvent) {
    const { direction, initialPosition, view } = this;
    const { nativeEvent, pointer } = event.listEvent;

    if (nativeEvent) {
      nativeEvent.preventDefault();
    }

    const delta = view.toLocalOffset(pointer.getDelta());
    const { xMin, xMax, yMin, yMax } = view.getPositionBounds();
    let { x, y } = initialPosition;

    if (direction !== 'vertical') {
      x -= delta.x;
      if (x > xMax) x = xMax + (x - xMax) * 0.5;
      if (x < xMin) x = xMin + (x - xMin) * 0.5;
    }

    if (direction !== 'horizontal') {
      y -= delta.y;
      if (y > yMax) y = yMax + (y - yMax) * 0.5;
      if (y < yMin) y = yMin + (y - yMin) * 0.5;
    }

    view.setPosition({ x, y });
  }

  private handleDragEnd(event: DragEvent) {
    const { view } = this;
    const { pointer } = event.listEvent;
    const velocity = this.getVelocity(pointer);

    this.preventNextClick = true;
    this.isDraging = false;

    if (view.isPositionPaged()) {
      const position = view.getPosition();
      position.x += velocity.x * 10;
      position.y += velocity.y * 10;
      view.tweenTo(view.clampPosition(position), { easing: easeOutExpo });
    } else {
      const { xMin, xMax, yMin, yMax } = view.getPositionBounds();

      momentum(view, {
        position: {
          velocity,
          min: { x: xMin, y: yMin },
          max: { x: xMax, y: yMax },
        },
      });
    }
  }

  handleWheel(event: WheelEvent) {
    if (this.isDraging || this.isDisabled()) {
      return;
    }

    const { direction, view } = this;
    const position = view.getPosition();
    let didUpdate: boolean = false;

    const delta = view.toLocalOffset({
      x: event.deltaX,
      y: event.deltaY,
    });

    if (direction !== 'vertical') {
      position.x += delta.x;
      didUpdate = didUpdate || Math.abs(event.deltaX) > 0;
    }

    if (direction !== 'horizontal') {
      position.y += delta.y;
      didUpdate = didUpdate || Math.abs(event.deltaY) > 0;
    }

    if (didUpdate) {
      event.preventDefault();
    }

    stop(view);
    view.setPosition(view.clampPosition(position));
  }
}
